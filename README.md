# COIN MARKET API

## Requirements
- chromedriver (http://chromedriver.chromium.org/downloads)
- node +10
- elasticsearch 6.5.3
- docker (if you need to run ES)


## LOCAL

### ElasticSearch  (if you need)
```
docker-compose up
```
#### Check if ES is online
http://localhost:9200/

### ENVIRONMENTS - Default 
- ELASTIC_SEARCH_URL = http://localhost:9200
- ELASTIC_SEARCH_USERNAME = user
- ELASTIC_SEARCH_PASSWORD = pass


### Run Command
```
npm run dev
```
#### API DOCS
- http://localhost:3000/api-docs


## General Info
```
Swagger in the project to make it more user-friendly.
After running it is possible to access it at the address: http://localhost:3000/api-docs.

It is necessary to run the crawler before searching for the coins and thus avoid the invalid index error.

To run the crawler via Swagger you can find it by searching for "start-crawler" or via CLI using the command: curl -X 'POST' \
   'http: // localhost: 3000 / coins / start-crawler' \
   -H 'accept: application / json' \
   -d ''.
```
