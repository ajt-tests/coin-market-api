import { Router } from 'express';
import CoinsController from '../controllers/coins.controller';
import Route from '../interfaces/routes.interface';


class CoinsRoute implements Route {
  public path = '/coins';
  public router: Router = Router();
  public coinsController = new CoinsController();

  constructor() {
    this.initializeRoutes();
  }

  private initializeRoutes() {
    this.router.get(this.path, this.coinsController.getCoins);
    this.router.get(`${this.path}/supply-availability`, this.coinsController.getCoinsBySupplyAvailability);
    this.router.get(`${this.path}/market-dominance`, this.coinsController.getCoinsByMarketDominance);
    this.router.get(`${this.path}/max-supply`, this.coinsController.getCoinByMaxSupply);
    this.router.post(`${this.path}/start-crawler`, this.coinsController.startCrawler);
  }
}

export default CoinsRoute;
