import { CronJob } from 'cron';
import CoinMarketCrawler from '../crawlers/coin-market.crawler';


class Scheduler {
  public static run() {
    
    new CronJob(
      '0 * * * *',
      () => {
          const coinMarket = new CoinMarketCrawler();
          coinMarket.start();
      },
      null,
      true,
      'America/Sao_Paulo'
    );
  }
}

export default Scheduler;
