import 'dotenv/config';
import App from './app';
import IndexRoute from './routes/index.route';
import CoinsRoute from './routes/coins.route';
import validateEnv from './utils/validateEnv';

validateEnv();

const app = new App([
  new IndexRoute(),
  new CoinsRoute()
]);

app.listen();
