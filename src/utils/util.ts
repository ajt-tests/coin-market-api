import { Coin } from '../interfaces/coin.interface';


export const isEmpty = (value: any): boolean => {
  if (value === null) {
    return true;
  } else if (typeof value !== 'number' && value === '') {
    return true;
  } else if (value === 'undefined' || value === undefined) {
    return true;
  } else if (value !== null && typeof value === 'object' && !Object.keys(value).length) {
    return true;
  } else {
    return false;
  }
};

export const parsePriceToNumber = (value: string): number => {
  return Number(value.replace('$', '').replace(/,/g, '0'));
};

export const parsePercentToNumber = (value: string): number => {
  return Number(value.replace('%', '').replace(/,/g, '0'));
};

export const parseStringToNumber = (value: string): number => {
  return Number(value.replace(/\D/g, ''));
};

export const parseMarketCapToNumber = (value: string): number => {
  // Check if the value is abbreviated
  if (value.search('T') !== -1) {
    const total = Number(value.replace('T', ''));
    return total * 10000000000;
  }

  return parseStringToNumber(value);
}

export const getAvailability = (coin: Coin): number => {
  try {
    return Number((coin.totalSupply / coin.maxSupply).toFixed(2));
  } catch (error) {
    return null;
  }
}
