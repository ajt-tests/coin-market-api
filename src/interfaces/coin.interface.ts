export interface Coin {
  position: number;
  name: string;
  symbol: string;
  price: number;
  marketCap: number;
  totalSupply: number;
  maxSupply: number;
  dominance: number | string;
  availability?: number;
}
