import { NextFunction, Request, Response } from 'express';
import ElasticSearchService from '../services/elasticsearch.service';
import CoinsService from '../services/coins.service';
import CoinMarketCrawler from '../crawlers/coin-market.crawler';
import { Coin } from '../interfaces/coin.interface';


class CoinsController {
  public elasticSearchService = new ElasticSearchService();
  public coinsService = new CoinsService();
  public coinMarketCrawler = new CoinMarketCrawler();

  public getCoins = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
      const search = {
        index: CoinMarketCrawler.INDEX,
        body: {
          sort: [{ 'position': { 'order': 'asc' } }],
          query: {
            match_all: {}
          }
        }
      }

      const data = await this.elasticSearchService.search(search);
      res.status(200).json(this.coinsService.parseBodyResult(data));
    } catch (error) {
      next(error);
    }
  };

  /**
   *
   a. What are the first 5 coins by supply availability? (total supply / max supply)
   */
  public getCoinsBySupplyAvailability = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
      const search = {
        index: CoinMarketCrawler.INDEX,
        size: 5,
        body: {
          sort: [{ 'availability': { 'order': 'desc' } }],
          query: {
            match_all: {}
          }
        },
      }

      const result = await this.elasticSearchService.search(search);
      res.status(200).json(this.coinsService.parseBodyResult(result));
    } catch (error) {
      next(error);
    }
  }

  public getCoinsByMarketDominance = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
      const search = {
        index: CoinMarketCrawler.INDEX,
        size: 5,
        body: {
          sort: [{ 'dominance': { 'order': 'desc' } }],
          query: {
            match_all: {}
          }
        }
      }

      const result = await this.elasticSearchService.search(search);
      let data = this.coinsService.parseBodyResult(result);
      data = data.map((item: Coin) => ({ ...item, dominance: `${item.dominance}%`}));
      res.status(200).json(data);
    } catch (error) {
      next(error);
    }
  }

  public getCoinByMaxSupply = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
      const search = {
        index: CoinMarketCrawler.INDEX,
        size: 5,
        body: {
          sort: [{ 'maxSupply': { 'order': 'desc' } }],
          query: {
            match_all: {}
          }
        }
      }

      const data = await this.elasticSearchService.search(search);
      res.status(200).json(this.coinsService.parseBodyResult(data));
    } catch (error) {
      next(error);
    }
  }

  public startCrawler = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
      const coins = await this.coinMarketCrawler.start();
      res.status(200).json(coins);
    } catch (error) {
      next(error);
    }
  }
}

export default CoinsController;
