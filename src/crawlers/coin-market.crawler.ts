import { By, WebElement } from 'selenium-webdriver';

import Crawler from '../abstracts/crawler.abstract';
import SeleniumService from '../services/selenium.service';
import ElasticSearchService from '../services/elasticsearch.service';
import { parsePriceToNumber, parseMarketCapToNumber, parsePercentToNumber, parseStringToNumber, getAvailability } from '../utils/util';
import { Coin } from '../interfaces/coin.interface';


class CoinMarketCrawler extends Crawler {
  public static URL = 'https://coinmarketcap.com';
  public static INDEX = 'coins';
  private rowsLimit = 10;

  public seleniumService = new SeleniumService();
  public elasticSearchService = new ElasticSearchService();

  constructor() {
    super();

    this.createIndexES();
  }

  public async start(): Promise<any> {
    const driver = await this.seleniumService.startDriver(CoinMarketCrawler.URL);
    driver.manage().window().maximize();
    await this.sleep(2000);
  
    try {
      await this.closeCookieInfo(driver);
      await this.customizeTable(driver);

      const rowsSelector = '//table[contains(@class, \'cmc-table\')]/tbody/tr';
      const rows = await driver.findElements(By.xpath(rowsSelector)) as WebElement[];

      const promises = rows.slice(0, this.rowsLimit).map(async (row: WebElement) => {
        const cols = await row.findElements(By.tagName('td')) as WebElement[];
        const colName = await cols[2].findElements(By.tagName('p')) as WebElement[];

        const coin: Coin = {
          position: Number(await cols[1].getText()),
          name: await colName[0].getText(),
          symbol: await colName[1].getText(),
          price: parsePriceToNumber(await cols[3].getText()),
          marketCap: parseMarketCapToNumber(await cols[6].getText()),
          totalSupply: parseStringToNumber(await cols[10].getText()),
          maxSupply: parseStringToNumber(await cols[11].getText()),
          dominance: parsePercentToNumber(await cols[12].getText()),
        };

        coin.availability = getAvailability(coin);

        return coin;
      });
      // Awaiting all promises
      const coins = await Promise.all(promises);
      await this.elasticSearchService.bulk(coins, CoinMarketCrawler.INDEX);
      return coins;
    } catch (error) {
      throw error;
    } finally {
      driver && await driver.close();
    }
  }

  private async closeCookieInfo(driver): Promise<boolean> {
    const cookieBannerSelector = '//div[contains(@class, \'cmc-cookie-policy-banner__close\')]';
    const cookieBannerButton = await driver.findElements(By.xpath(cookieBannerSelector)) as WebElement[];
    cookieBannerButton[0] && await cookieBannerButton[0].click();

    await this.sleep(500);

    return true;
  }

  private async customizeTable(driver): Promise<boolean> {
    await this.sleep(2000);
    const customizeBtnSelector = '//button[contains(child::text(), "Customize")]';
    const buttons = await driver.findElements(By.xpath(customizeBtnSelector)) as WebElement[];
    if (buttons.length === 0) {
      return false;
    }

    const btnCustomize = buttons[1];
    await btnCustomize.click();

    await this.sleep(1000);

    await this.selectColumn(driver, 'Total Supply');
    await this.selectColumn(driver, 'Max Supply');
    await this.selectColumn(driver, 'Dominance %');

    const applyBtnSelector = '//button[contains(child::text(), "Apply Changes")]';
    const applyButton = await driver.findElements(By.xpath(applyBtnSelector)) as WebElement[];
    if (buttons.length === 0) {
      return false;
    }

    applyButton[0] && await applyButton[0].click();

    await this.sleep(1000);

    return true;
  }

  private async selectColumn(driver, columnName: string): Promise<void> {
    const selector = `//span[contains(child::text(), "${columnName}")]`;
    const element = await driver.findElement(By.xpath(selector)) as WebElement;
    driver.executeScript("arguments[0].scrollIntoView()", element);
    driver.sleep(300);
    element && element.click();

    await this.sleep(500);
  } 

  private async createIndexES(): Promise<any> {
    const index = {
      index: CoinMarketCrawler.INDEX,
      body: {
        mappings: {
          properties: {
            position: { type: 'integer' },
            name: { type: 'text' },
            symbol: { type: 'text' },
            price: { type: 'float' },
            marketCap: { type: 'float' },
            totalSupply: { type: 'float' },
            maxSupply: { type: 'float' },
            dominance: { type: 'float' },
            availability: { type: 'float' },
          }
        }
      }
    };

    await this.elasticSearchService.createIndex(index);
  }
}

export default CoinMarketCrawler;
