abstract class Crawler {
  abstract start(): void;

  protected sleep(ms): Promise<any> {
    return new Promise( resolve => setTimeout(resolve, ms) );
  }
}

export default Crawler;
