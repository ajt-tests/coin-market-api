import { Client } from '@elastic/elasticsearch';


class ElasticSearchService {
  public static URL = process.env.ELASTIC_SEARCH_URL || 'http://localhost:9200';
  public static username = process.env.ELASTIC_SEARCH_USERNAME || 'user';
  public static password = process.env.ELASTIC_SEARCH_PASSWORD || 'pass';

  private client: Client;

  constructor() {
    this.client = this.getClient();
  }

  private getClient(): Client {
    return new Client({
      node: ElasticSearchService.URL,
      auth: {
        username: ElasticSearchService.username,
        password: ElasticSearchService.password
      }
    })
  }

  public async search(search): Promise<any> {
    return await this.client.search(search);
  }

  public async createIndex(index): Promise<any> {
    const result = await this.client.indices.create(index, { ignore: [400, 404] });
    return result;
  }

  public async bulk(dataset, index): Promise<any> {
    const body = dataset.flatMap(doc => [{ index: { _index: index, _type: index } }, doc]);
    const { body: bulkResponse } = await this.client.bulk({ body, refresh: true });
    return bulkResponse;
  }

  public async count(index): Promise<any> {
    const { body: count } = await this.client.count({ index });
    return count;
  }

  public async deleteIndex(index): Promise<any> {
    const data = await this.client.indices.delete({ index }, { ignore: [400, 404]});
    return data;
  }
}

export default ElasticSearchService;
