import chrome from 'selenium-webdriver/chrome';
import { Builder } from 'selenium-webdriver';


class SeleniumService {

  public async startDriver(url: string): Promise<any> {
    try {
      let driver = await new Builder()
        .forBrowser('chrome')
        .setChromeOptions(new chrome.Options()
          .headless()
          .windowSize({ width: 1200, height: 800 })
          .addArguments(`user-agent=Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36`)
          .addArguments('--no-sandbox'))

        .build();

      await driver.get(url);
      return driver;
    } catch (error) {
      throw error;
    }
  }
}

export default SeleniumService;
