import { Coin } from '../interfaces/coin.interface';


class CoinsService {
  public parseBodyResult(data): Coin[] {
    const { body: { hits: { hits } } } = data;
    return hits.map(item => item._source) as Coin[];
  }
}

export default CoinsService;
