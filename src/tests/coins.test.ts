import request from 'supertest';
import App from '../app';
import { Coin } from '../interfaces/coin.interface';
import CoinsRoute from '../routes/coins.route';

afterAll(async () => {
  await new Promise<void>(resolve => setTimeout(() => resolve(), 500));
});

describe('Testing Coins', () => {
  describe('[GET] /coins', () => {
    it('response statusCode 200 / findAll', () => {
      const findCoin: Coin[] = [{
        'position': 1,
        'name': 'Bitcoin',
        'symbol': 'BTC',
        'price': 570631.3,
        'marketCap': 1075128963577,
        'totalSupply': 18660331,
        'maxSupply': 21000000
      }];
      const coinsRoute = new CoinsRoute();
      const app = new App([coinsRoute]);

      return request(app.getServer()).get(`${coinsRoute.path}`).expect(200, { data: findCoin, message: 'findAll' });
    });
  });
});
